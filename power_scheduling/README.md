# Nutanix Playbook & Calm Runbook to Power Control VMs

This sample workflow utilizes four separate Playbooks and one Calm Runbook. The initial trigger playbook runs and executes the Calm Runbook providing a comma separated string in variable `category_str_input` containing one or more category:value pairs and the desired power state in another variable `desired_power_state`. 

When executed, the Calm Runbook checks that the inputs are valid, finds a list of all VMs matching **all** the provided categories and values pairs and the VMs current power state. If the current power state of the VM does not match the desired power state provided to the Runbook the corresponding playbook is called to modify the VMs power state.

```mermaid
flowchart LR
A[[Trigger Playbook]] -- HTTP REST --> B[[Calm Runbook]]
B -- HTTP REST --> D[[Webhook Playbook]]
```

## Setup

 - Import playbooks `playbook_webhook_power_off_vm.pbk`, `playbook_webhook_power_on_vm.pbk`,  `playbook_trigger_manual_power_off.pbk`, and `playbook_trigger_manual_power_on.pbk`.
	 - The sample Playbooks are set with a manual trigger for testing.
	 - Within the HTTP body in the trigger Playbooks update the comma separated list of categories and values in the variable named `category_str_input` to match your requirements. This should be in the format `category1:value1,category2:value2,categoryX:valueY`
	 - Record the UUID of each of the webhook playbooks for use in a future step.
 - Import Calm Runbook.
 - Update `power_off_playbook_webhook_uuid` and `power_on_playbook_webhook_uuid` variables within the Calm Runbook with the corresponding webhook UUIDs recorded earlier.
 - Get the webhook UUID and record for use in a future step. This can easily be seen in the Calm UI by hovering over the link to view the Runbook.
 - Update the URL in any of the trigger playbooks with the correct Runbook UUID recorded earlier.

## Post-Setup Configuration

 - Clone the sample trigger playbooks and consider using a Time trigger event to have this automatically run at a time of your choosing.
 - Add actions (to either the trigger or webhook playbooks) for additional actions needed during this process. Some examples could be;
	 - On webhook playbook;
		 - Push a notification to Microsoft Teams or Slack that a power control event is occurring on a specific VM.
	 - On trigger playbook;
		 - Notify users that a category of VMs is undergoing power control.
		 - Add a short wait period before calling the Calm Runbook to allow someone to change their VM power control category if they have critical work ongoing.
		 - 

# Process Flowchart
```mermaid
flowchart TB

	a2 -->|HTTP REST| calm-runbook
	b10 -->|HTTP REST| playbook-webhook-power-on
	b12 -->|HTTP REST| playbook-webhook-power-off
	
    subgraph playbook-action
    a1([Start]) --> a2[Calm Runbook]
    a3[[Category String & Power State]] -->|Provided| a2
    a2 --> a4([End])
    end
    
    subgraph calm-runbook
    b1([Start]) --> b2[Check Category & Values]
    b2 --> b3{Category & Values Exist}
	b3 -->|Yes| b4[Get VMs matching categories]
	b3 -->|No| b13([End])
	b4 --> b5[Start Loop]
	b5 --> b6[Get VM Power State]
	b6 --> b7{VM Powered On}
	b7 -->|Yes| b8{Desired State = ON}
	b8 -->|Yes| b9
	b8 -->|No| b10[Call Playbook Webhook] --> b9
	b7 -->|No| b11{Desired State = OFF}
	b11 -->|Yes| b9
	b11 -->|No| b12[Call Playbook Webhook] --> b9
	b9[End Loop] -->|Next VM| b5
	b9 -->|No More VMs| b13(End)
    end

    subgraph playbook-webhook-power-off
    c1([Start]) --> c2[Power Off VM]
    c3[[VM Entity]] -->|Provided| c1
    c2 --> c4([End])
    end
    
    subgraph playbook-webhook-power-on
    d1([Start]) --> d2[Power On VM]
    d3[[VM Entity]] -->|Provided| d1
    d2 --> d4([End])
    end
```